# Translation of plasma_shell_org.kde.plasma__desktop.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2013, 2014, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasma_shell_org.kde.plasma__desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-30 03:36+0000\n"
"PO-Revision-Date: 2017-09-25 19:53+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: contents/activitymanager/ActivityItem.qml:212
msgid "Currently being used"
msgstr "Тренутно се користи"

#: contents/activitymanager/ActivityItem.qml:252
#, fuzzy
#| msgid "Stop activity"
msgid ""
"Move to\n"
"this activity"
msgstr "Заустави активност"

#: contents/activitymanager/ActivityItem.qml:282
msgid ""
"Show also\n"
"in this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:344
msgid "Configure"
msgstr "Подеси"

#: contents/activitymanager/ActivityItem.qml:363
msgid "Stop activity"
msgstr "Заустави активност"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "Заустављене активности:"

#: contents/activitymanager/ActivityManager.qml:127
#, fuzzy
#| msgid "Create activity..."
msgid "Create activity…"
msgstr "Направи активност..."

#: contents/activitymanager/Heading.qml:61
msgid "Activities"
msgstr "Активности"

#: contents/activitymanager/StoppedActivityItem.qml:135
msgid "Configure activity"
msgstr "Подеси активност"

#: contents/activitymanager/StoppedActivityItem.qml:152
msgid "Delete"
msgstr "Обриши"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr ""

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr ""

#: contents/applet/CompactApplet.qml:71
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "Лиценца:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr ""

#: contents/configuration/AboutPlugin.qml:160
#, fuzzy
#| msgid "Author:"
msgid "Authors"
msgstr "Аутор:"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr ""

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:56
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Keyboard Shortcuts"
msgstr "Пречице са тастатуре"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "Примени поставке"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Измењене су поставке текућег модула. Желите ли да их примените или одбаците?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "У реду"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Примени"

#: contents/configuration/AppletConfiguration.qml:338
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "Одустани"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Лево дугме"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Десно дугме"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Средње дугме"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Дугме назад"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Дугме напред"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Усправно клизање"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Водоравно клизање"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Додај радњу"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "Распоред:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
#, fuzzy
#| msgid "Wallpaper Type:"
msgid "Wallpaper type:"
msgstr "Тип тапета:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr "Промене распореда морају да се примене пре даљих измена."

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
#, fuzzy
#| msgid "Apply now"
msgid "Apply Now"
msgstr "Примени сада"

#: contents/configuration/ConfigurationShortcuts.qml:16
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Shortcuts"
msgstr "Пречице са тастатуре"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Тапет"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Радње мишем"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Унос овде"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:41
#, fuzzy
#| msgid "Apply Settings"
msgid " Panel Settings"
msgstr "Примени поставке"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:47
#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Уклони панел"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:71
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment:"
msgstr "Поравнање панела"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:77
msgid "Top"
msgstr "Врх"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:77
msgid "Left"
msgstr "Лево"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:78
msgid "Aligns the panel to the top if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:78
msgid "Aligns the panel"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:87
msgid "Center"
msgstr "Средина"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:88
msgid "Center aligns the panel if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid "Bottom"
msgstr "Дно"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid "Right"
msgstr "Десно"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:97
msgid "Aligns the panel to the bottom if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:97
msgid "Aligns the panel to the right if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:130
#, fuzzy
#| msgid "Visibility"
msgid "Visibility:"
msgstr "Видљивост"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:135
msgid "Always Visible"
msgstr "Увек видљив"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:136
msgid "Makes the panel remain visible always."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:144
msgid "Auto Hide"
msgstr "Аутоматско сакривање"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:145
msgid ""
"Makes the panel hidden always but reveals it when mouse enters the area "
"where the panel would have been if it were not hidden."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:164
#, fuzzy
#| msgid "Windows Can Cover"
msgid "Windows In Front"
msgstr "Прозори могу да прекрију"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:165
msgid ""
"Makes the panel remain visible always but maximized windows shall cover it. "
"It is revealed when mouse enters the area where the panel would have been if "
"it were not covered."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:175
#, fuzzy
#| msgid "Windows Go Below"
msgid "Windows Behind"
msgstr "Прозори иду испод"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:176
msgid ""
"Makes the panel remain visible always but part of the maximized windows "
"shall go below the panel as though the panel did not exist."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:205
msgid "Opacity:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:210
msgid "Fully Opaque"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:211
msgid "Makes the panel opaque always."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:219
msgid "Adaptive"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:220
msgid "Makes the panel translucent except when some windows touch it."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:229
msgid "Translucent"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:230
msgid "Makes the panel translucent always."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:259
msgid "Floating:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:264
msgid "Floating"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:265
msgid "Makes the panel float from the edge of the screen."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:274
msgid "Attached"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:275
msgid "Makes the panel remain attached to the edge of the screen."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:297
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Focus Shortcut"
msgstr "Пречице са тастатуре"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:307
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:24
#, fuzzy
#| msgid "Add Widgets..."
msgid "Add Widgets…"
msgstr "Додај виџете..."

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "Додај размакницу"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
#, fuzzy
#| msgid "More Settings..."
msgid "More Options…"
msgstr "Више поставки..."

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:282
#, fuzzy
#| msgid "Panel Alignment"
msgid "Panel height:"
msgstr "Поравнање панела"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Затвори"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Уклони панел"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "Алтернативни виџети"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "Пребаци"

#: contents/explorer/AppletDelegate.qml:177
msgid "Undo uninstall"
msgstr "Опозови деинсталирање"

#: contents/explorer/AppletDelegate.qml:178
msgid "Uninstall widget"
msgstr "Деинсталирај виџет"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "Аутор:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "Е‑пошта:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "Деинсталирај"

#: contents/explorer/WidgetExplorer.qml:129
#: contents/explorer/WidgetExplorer.qml:240
#, fuzzy
#| msgid "Widgets"
msgid "All Widgets"
msgstr "Виџети"

#: contents/explorer/WidgetExplorer.qml:194
msgid "Widgets"
msgstr "Виџети"

#: contents/explorer/WidgetExplorer.qml:202
#, fuzzy
#| msgid "Get new widgets"
msgid "Get New Widgets…"
msgstr "Добави нове виџете"

#: contents/explorer/WidgetExplorer.qml:251
msgid "Categories"
msgstr "Категорије"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets matched the search terms"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets available"
msgstr ""

#~ msgid "Maximize Panel"
#~ msgstr "Максимизуј панел"

#~ msgid "Search..."
#~ msgstr "Тражи..."

#~ msgid "Screen Edge"
#~ msgstr "Ивица екрана"
