# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Dimitris Kardarakos <dimkard@gmail.com>, 2014, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2016-10-31 15:37+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgid "Top"
msgstr ""

#: ui/main.qml:40
#, kde-format
msgid "Center"
msgstr ""

#: ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr ""

#: ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr ""

#: ui/main.qml:69
#, fuzzy, kde-format
#| msgid "Clear History"
msgid "History:"
msgstr "Καθαρισμός ιστορικού"

#: ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr ""

#: ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr ""

#: ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr ""

#: ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr "Καθαρισμός ιστορικού"

#: ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr ""

#: ui/main.qml:111
#, fuzzy, kde-format
#| msgid "Clear History"
msgid "Clear History…"
msgstr "Καθαρισμός ιστορικού"

#: ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr ""

#: ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr ""

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr ""

#: ui/main.qml:174
#, fuzzy, kde-format
#| msgid "Configure Search"
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Διαμόρφωση αναζήτησης"

#~ msgid "Available Plugins"
#~ msgstr "Διαθέσιμα πρόσθετα"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Δημήτρης Καρδαράκος"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "dimkard@gmail.com"

#, fuzzy
#~| msgid "Configure Search"
#~ msgctxt "kcm name for About dialog"
#~ msgid "Configure search settings"
#~ msgstr "Διαμόρφωση αναζήτησης"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "KRunner history:"
#~ msgstr "Καθαρισμός ιστορικού"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "Clear History..."
#~ msgstr "Καθαρισμός ιστορικού"

#, fuzzy
#~| msgid "Select the search plugins"
#~ msgid "Select the search plugins:"
#~ msgstr "Επιλογή των προσθέτων αναζήτησης"
