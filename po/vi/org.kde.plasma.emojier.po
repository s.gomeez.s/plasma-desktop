# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-06 02:09+0000\n"
"PO-Revision-Date: 2022-10-01 18:03+0200\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.04.1\n"

#: app/emojier.cpp:83 app/emojier.cpp:85
#, kde-format
msgid "Emoji Selector"
msgstr "Trình chọn hình biểu cảm"

#: app/emojier.cpp:87
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:89
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Nguyễn Hùng Phú"

#: app/emojier.cpp:89
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "phu.nguyen@kdemail.net"

#: app/emojier.cpp:104
#, kde-format
msgid "Replace an existing instance"
msgstr "Thay thế một hiện thể đã có"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:50
#, kde-format
msgid "Search"
msgstr "Tìm kiếm"

#: app/ui/CategoryPage.qml:80
#, kde-format
msgid "Clear History"
msgstr "Xoá lịch sử"

#: app/ui/CategoryPage.qml:104
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Character"
msgstr ""

#: app/ui/CategoryPage.qml:107 app/ui/CategoryPage.qml:115
#: app/ui/emojier.qml:34
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 được chép vào bảng nháp"

#: app/ui/CategoryPage.qml:112
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Description"
msgstr ""

#: app/ui/CategoryPage.qml:205
#, kde-format
msgid "No recent Emojis"
msgstr "Không có hình biểu cảm nào gần đây"

#: app/ui/emojier.qml:40
#, kde-format
msgid "Recent"
msgstr "Gần đây"

#: app/ui/emojier.qml:61
#, kde-format
msgid "All"
msgstr "Tất cả"

#: app/ui/emojier.qml:72
#, kde-format
msgid "Categories"
msgstr "Các loại"

#: emojicategory.cpp:14
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr "Hình biểu cảm và cảm xúc"

#: emojicategory.cpp:15
msgctxt "Emoji Category"
msgid "People and Body"
msgstr "Con người và cơ thể"

#: emojicategory.cpp:16
msgctxt "Emoji Category"
msgid "Component"
msgstr "Thành phần"

#: emojicategory.cpp:17
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr "Động vật và thiên nhiên"

#: emojicategory.cpp:18
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr "Đồ ăn thức uống"

#: emojicategory.cpp:19
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr "Du lịch và địa điểm"

#: emojicategory.cpp:20
msgctxt "Emoji Category"
msgid "Activities"
msgstr "Hoạt động"

#: emojicategory.cpp:21
msgctxt "Emoji Category"
msgid "Objects"
msgstr "Vật thể"

#: emojicategory.cpp:22
msgctxt "Emoji Category"
msgid "Symbols"
msgstr "Kí hiệu"

#: emojicategory.cpp:23
msgctxt "Emoji Category"
msgid "Flags"
msgstr "Cờ"

#~ msgid "Search…"
#~ msgstr "Tìm kiếm…"

#~ msgid "Search..."
#~ msgstr "Tìm kiếm…"
