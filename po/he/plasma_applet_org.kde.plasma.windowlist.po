# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.windowlist\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-29 00:59+0000\n"
"PO-Revision-Date: 2017-05-16 06:56-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Hebrew\n"
"X-Poedit-Country: ISRAEL\n"
"X-Generator: Zanata 3.9.6\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/ConfigGeneral.qml:27
#, kde-format
msgid "Show active application's name on Panel button"
msgstr ""

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Only icons can be shown when the Panel is vertical."
msgstr ""

#: contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Not applicable when the widget is on the Desktop."
msgstr ""

#: contents/ui/main.qml:86
#, kde-format
msgid "Plasma Desktop"
msgstr ""

#~ msgid "Show list of opened windows"
#~ msgstr "הצג רשימה של חלונות פתוחים"

#~ msgid "On all desktops"
#~ msgstr "על כל שולחנות העבודה"

#~ msgid "Window List"
#~ msgstr "רשימת חלונות"
