# Uyghur translation for joystick.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sahran <sahran.ug@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: joystick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2013-09-08 07:05+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#: caldialog.cpp:26 joywidget.cpp:339
#, kde-format
msgid "Calibration"
msgstr "توغرىلاش"

#: caldialog.cpp:40
#, kde-format
msgid "Next"
msgstr "كېيىنكى"

#: caldialog.cpp:50
#, kde-format
msgid "Please wait a moment to calculate the precision"
msgstr "سەل كۈتۈڭ ئېنىقلىقىنى ھېسابلاۋاتىدۇ"

#: caldialog.cpp:79
#, kde-format
msgid "(usually X)"
msgstr "(ھەمىشە X)"

#: caldialog.cpp:81
#, kde-format
msgid "(usually Y)"
msgstr "(ھەمىشە Y)"

#: caldialog.cpp:87
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>minimum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>تۈزىتىش ئۈسكۈنىڭىز چىقارغان قىممەت دائىرىسىنى توغرىلايدۇ.<br /><br /"
">ئۈسكۈنىڭىزدىكى <b> %1 %2 ئوق</b>نى <b>ئەڭ كىچىك</b> قىممەت ئورنىغا يۆتكەڭ."
"<br /><br /> ئۈسكۈنىدىكى خالىغان توپچا بېسىلسا ياكى «كېيىنكى» توپچىسى "
"بېسىلسا كېيىنكى تەكشۈرۈشنى داۋاملاشتۇرىدۇ.</qt>"

#: caldialog.cpp:110
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>center</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>تۈزىتىش ئۈسكۈنىڭىز چىقارغان قىممەت دائىرىسىنى توغرىلايدۇ.<br /><br /"
">ئۈسكۈنىڭىزدىكى <b> %1 %2 ئوق</b>نى <b>ئوتتۇرا</b> قىممەت ئورنىغا يۆتكەڭ."
"<br /><br /> ئۈسكۈنىدىكى خالىغان توپچا بېسىلسا ياكى «كېيىنكى» توپچىسى "
"بېسىلسا كېيىنكى تەكشۈرۈشنى داۋاملاشتۇرىدۇ.</qt>"

#: caldialog.cpp:133
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>maximum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>تۈزىتىش ئۈسكۈنىڭىز چىقارغان قىممەت دائىرىسىنى توغرىلايدۇ.<br /><br /"
">ئۈسكۈنىڭىزدىكى <b> %1 %2 ئوق</b>نى <b>ئەڭ چوڭ</b> قىممەت ئورنىغا يۆتكەڭ."
"<br /><br /> ئۈسكۈنىدىكى خالىغان توپچا بېسىلسا ياكى «كېيىنكى» توپچىسى "
"بېسىلسا كېيىنكى تەكشۈرۈشنى داۋاملاشتۇرىدۇ.</qt>"

#: caldialog.cpp:160 joywidget.cpp:329 joywidget.cpp:365
#, kde-format
msgid "Communication Error"
msgstr "ئالاقە خاتالىقى"

#: caldialog.cpp:164
#, kde-format
msgid "You have successfully calibrated your device"
msgstr "سىز ئۈسكۈنىڭىزنى مۇۋەپپەقىيەتلىك توغرىلىدىڭىز"

#: caldialog.cpp:164 joywidget.cpp:367
#, kde-format
msgid "Calibration Success"
msgstr "توغرىلاش مۇۋەپپەقىيەتلىك بولدى"

#: caldialog.cpp:184
#, kde-format
msgid "Value Axis %1: %2"
msgstr "ئوق قىممىتى %1: %2"

#: joydevice.cpp:41
#, kde-format
msgid "The given device %1 could not be opened: %2"
msgstr "بېرىلگەن ئۈسكۈنە %1 نى ئاچالمىدى: %2"

#: joydevice.cpp:45
#, kde-format
msgid "The given device %1 is not a joystick."
msgstr "بېرىلگەن ئۈسكۈنە %1 ئويۇن دەستىسى ئەمەس."

#: joydevice.cpp:49
#, kde-format
msgid "Could not get kernel driver version for joystick device %1: %2"
msgstr "ئويۇن دەستە ئۈسكۈنە %1 نىڭ يادرو قوزغاتقۇ نەشرى: %2"

#: joydevice.cpp:60
#, kde-format
msgid ""
"The current running kernel driver version (%1.%2.%3) is not the one this "
"module was compiled for (%4.%5.%6)."
msgstr ""
"نۆۋەتتە ئىجرا قىلىنىۋاتقان يادرو قوزغاتقۇ نەشرى (%1.%2.%3) بۇ تەرجىمە-تەھرىر "
"مەزكۇر بۆلەك نەشرى ئەمەس (%4.%5.%6)."

#: joydevice.cpp:71
#, kde-format
msgid "Could not get number of buttons for joystick device %1: %2"
msgstr "ئويۇن دەستە ئۈسكۈنىسى %1 نىڭ تۈگمە سانىغا ئېرىشەلمىدى: %2"

#: joydevice.cpp:75
#, kde-format
msgid "Could not get number of axes for joystick device %1: %2"
msgstr "ئويۇن دەستە ئۈسكۈنىسى %1 نىڭ ئوق سانىغا ئېرىشەلمىدى: %2"

#: joydevice.cpp:79
#, kde-format
msgid "Could not get calibration values for joystick device %1: %2"
msgstr "ئويۇن دەستە ئۈسكۈنىسى %1 نىڭ توغرىلاش قىممىتىگە ئېرىشەلمىدى: %2"

#: joydevice.cpp:83
#, kde-format
msgid "Could not restore calibration values for joystick device %1: %2"
msgstr ""
"ئويۇن دەستە ئۈسكۈنىسى %1 نىڭ توغرىلاش قىممىتىنى ئەسلىگە كەلتۈرەلمىدى: %2"

#: joydevice.cpp:87
#, kde-format
msgid "Could not initialize calibration values for joystick device %1: %2"
msgstr "ئويۇن دەستە ئۈسكۈنىسى %1 نىڭ توغرىلاش قىممىتىنى دەسلەپلەشتۈرەلمىدى: %2"

#: joydevice.cpp:91
#, kde-format
msgid "Could not apply calibration values for joystick device %1: %2"
msgstr "ئويۇن دەستە ئۈسكۈنىسى %1 نىڭ توغرىلاش قىممىتىنى قوللىنالمىدى: %2"

#: joydevice.cpp:95
#, kde-format
msgid "internal error - code %1 unknown"
msgstr "ئىچكى خاتالىق - كودى %1 نامەلۇم"

#: joywidget.cpp:67
#, kde-format
msgid "Device:"
msgstr "ئۈسكۈنە:"

#: joywidget.cpp:84
#, kde-format
msgctxt "Cue for deflection of the stick"
msgid "Position:"
msgstr "ئورنى:"

#: joywidget.cpp:87
#, kde-format
msgid "Show trace"
msgstr "ئىزلاشنى كۆرسەت"

#: joywidget.cpp:96 joywidget.cpp:303
#, kde-format
msgid "PRESSED"
msgstr "بېسىلغان"

#: joywidget.cpp:98
#, kde-format
msgid "Buttons:"
msgstr "توپچىلار:"

#: joywidget.cpp:102
#, kde-format
msgid "State"
msgstr "ھالەت"

#: joywidget.cpp:110
#, kde-format
msgid "Axes:"
msgstr "ئوقلار:"

#: joywidget.cpp:114
#, kde-format
msgid "Value"
msgstr "قىممەت"

#: joywidget.cpp:127
#, kde-format
msgid "Calibrate"
msgstr "توغرىلا"

#: joywidget.cpp:190
#, kde-format
msgid ""
"No joystick device automatically found on this computer.<br />Checks were "
"done in /dev/js[0-4] and /dev/input/js[0-4]<br />If you know that there is "
"one attached, please enter the correct device file."
msgstr ""
"بۇ كومپيۇتېردا ئويۇن دەستە ئۈسكۈنىسى بايقالمىدى. <br /> بايىلا /dev/js[0-4] "
"ۋە /dev/input/js[0-4] تەكشۈرۈلدى<br /> ئەگەر ئويۇن دەستىسىنى چېتىپ بولغان "
"بولسىڭىز، توغرا ئۈسكۈنە ھۆججىتىنى كىرگۈزۈڭ."

#: joywidget.cpp:226
#, kde-format
msgid ""
"The given device name is invalid (does not contain /dev).\n"
"Please select a device from the list or\n"
"enter a device file, like /dev/js0."
msgstr ""
"بېرىلگەن ئۈسكۈنە ئاتى ئىناۋەتسىز (/dev نى ئۆز ئىچىگە ئالمايدۇ).\n"
"تىزىمدىن ئۈسكۈنىنى تاللاڭ ياكى ئۈسكۈنە ھۆججىتىنى كىرگۈزۈڭ، مەسىلەن، /dev/js0"

#: joywidget.cpp:229
#, kde-format
msgid "Unknown Device"
msgstr "نامەلۇم ئۈسكۈنە"

#: joywidget.cpp:247
#, kde-format
msgid "Device Error"
msgstr "ئۈسكۈنە خاتالىقى"

#: joywidget.cpp:265
#, kde-format
msgid "1(x)"
msgstr "1(x)"

#: joywidget.cpp:266
#, kde-format
msgid "2(y)"
msgstr "2(y)"

#: joywidget.cpp:335
#, kde-format
msgid ""
"<qt>Calibration is about to check the precision.<br /><br /><b>Please move "
"all axes to their center position and then do not touch the joystick anymore."
"</b><br /><br />Click OK to start the calibration.</qt>"
msgstr ""
"<qt>تەكشۈرىدىغان ئېنىقلىقنى توغرىلاڭ.<br /><br /><b>ھەممە ئوقنى مەركەز "
"ئورنىغا يۆتكەپ ئاندىن ئويۇن دەستىسىگە چېقىلماڭ.</b><br /><br />جەزملە "
"چېكىلسە توغرىلاشنى باشلايدۇ.</qt>"

#: joywidget.cpp:367
#, kde-format
msgid "Restored all calibration values for joystick device %1."
msgstr "ئويۇن دەستە ئۈسكۈنىسى %1 ھەممە توغرىلاش قىممىتىنى ئەسلىگە كەلتۈردى."

#~ msgid "KDE Joystick Control Module"
#~ msgstr "KDE ئويۇن دەستە تىزگىن بۆلىكى"

#~ msgid "KDE System Settings Module to test Joysticks"
#~ msgstr "ئويۇن دەستىسىنى سىنايدىغان KDE سىستېما تەڭشەك بۆلىكى"

#~ msgid "(c) 2004, Martin Koller"
#~ msgstr "(c) 2004, Martin Koller"

#~ msgid ""
#~ "<h1>Joystick</h1>This module helps to check if your joystick is working "
#~ "correctly.<br />If it delivers wrong values for the axes, you can try to "
#~ "solve this with the calibration.<br />This module tries to find all "
#~ "available joystick devices by checking /dev/js[0-4] and /dev/input/"
#~ "js[0-4]<br />If you have another device file, enter it in the combobox."
#~ "<br />The Buttons list shows the state of the buttons on your joystick, "
#~ "the Axes list shows the current value for all axes.<br />NOTE: the "
#~ "current Linux device driver (Kernel 2.4, 2.6) can only "
#~ "autodetect<ul><li>2-axis, 4-button joystick</li><li>3-axis, 4-button "
#~ "joystick</li><li>4-axis, 4-button joystick</li><li>Saitek Cyborg "
#~ "'digital' joysticks</li></ul>(For details you can check your Linux source/"
#~ "Documentation/input/joystick.txt)"
#~ msgstr ""
#~ "<h1>ئويۇن دەستىسى</h1>بۇ بۆلەك ئويۇن دەستىسىنىڭ توغرا خىزمەت قىلغان ياكى "
#~ "قىلمىغانلىقىنى تەكشۈرۈشىڭىزگە ياردەم بېرىدۇ.<br />ئەگەر ئويۇن دەستىسى "
#~ "ئوقى يوللىغان قىممەتتە خاتالىق كۆرۈلسە، تۈزىتىش ئارقىلىق ھەل قىلسىڭىز "
#~ "بولىدۇ.<br />بۇ بۆلەك /dev/js[0-4] ۋە /dev/input/js[0-4] غا ئۇلانغان "
#~ "ئويۇن دەستىلىرىنىڭ ھەممىسىنى ئىزدەشنى سىنايدۇ<br />ئەگەر باشقا ئۈسكۈنە "
#~ "ھۆججىتىڭىز بولسا، بىرىكمە رامكىغا كىرگۈزۈڭ.<br />تۈگمە تىزىمى ئويۇن "
#~ "دەستىڭىزدىكى تۈگمە ھالىتىنى كۆرسىتىدۇ، ئوق تىزىمى ھەممە ئوقنىڭ نۆۋەتتىكى "
#~ "ھالەت قىممىتىنى كۆرسىتىدۇ.<br />دىققەت: نۆۋەتتىكى Linux ئۈسكۈنە قوزغاتقۇ "
#~ "(يادرو 2.4, 2.6) پەقەت<ul><li>ئىككى ئوقلۇق تۆت تۈگمىلىك ئويۇن دەستىسى</"
#~ "li><li>ئۈچ ئوقلۇق تۆت تۈگمىلىك ئويۇن دەستىسى</li><li>تۆت ئوقلۇق تۆت "
#~ "تۈگمىلىك ئويۇن دەستىسى</li><li>Saitek Cyborg رەقەملىك ئويۇن دەستىسى</"
#~ "li>نى ئۆزلۈكىدىن بايقىيالايدۇ</ul>(مۇناسىۋەتلىك تەپسىلاتنى Linux ئەسلى "
#~ "كودى /Documentation/input/joystick.txt نى تەكشۈرۈڭ)"
